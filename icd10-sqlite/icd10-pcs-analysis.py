# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# This is scratchpad type code so little in the way of comments or explanations. It is being used to explore the XML data prior to creating a schema and program to convert XML to sqlite db (separate notebook/py file).

# <headingcell level=1>

# SET UP

# <codecell>

import xml.etree.cElementTree as ET

# <codecell>

tree = ET.parse("icd10pcs_tabular_2014.xml")
root = tree.getroot()

# <headingcell level=1>

# UTILITIES

# <codecell>

def unique(tag, target=None):
    """Returns unique set, len, total"""
    uniq = target if target else set()
    i = 0
    for i,e in enumerate(root.iterfind(tag)):
        uniq.add(e.text) # No test!
    return uniq, len(uniq), i
    
def iter_len(tag):
    """Memory efficient len() for iterator"""
    return sum(1 for _ in root.iterfind(tag))

# <headingcell level=1>

# QUICK CHECKS

# <codecell>

# Check our xpaths

# Total Tables and Table-Axes
ntables = iter_len(".//pcsTable")
ntable_axes = iter_len(".//pcsTable/axis")
print "We have %s tables so should have %s x 3 = %s table axes: %s" % (ntables, ntables, 3*ntables, ntable_axes)

# Total Row and Row-Axes
nrows = iter_len(".//pcsRow")
nrow_axes = iter_len(".//pcsRow/axis")
print "We have %s rows so should have %s x 4 = %s row axes: %s" % (nrows, nrows, 4*nrows, nrow_axes)

# Total Axes
naxes = iter_len(".//axis")
print "So now we should have %s + %s = %s axes: %s" % (ntable_axes, nrow_axes, ntable_axes+nrow_axes, naxes)

# Total Titles: db title + naxes
print "Total titles (naxes + 1 for db) : %s" % iter_len(".//title")

# Cannot pre-calc total labels (varies per row-axis)...

# Total Labels
print "Total labels : %s" % iter_len(".//label")

# <headingcell level=1>

# TITLE & LABEL REDUNDANCY PER AXIS

# <codecell>

def axis_stats():
    """Determine redundancy of titles and labels for each axis type (1..7)"""
    
    print "(axis type, unique #, total #, redundancy %)"
    
    unique_total = 0
    for tag in ("title", "label"):
        print "\n%s:\n" % tag
        for n in range(1,8):
            u,i,t = unique(".//axis[@pos='%s']/%s" % (n, tag))
            print n, i, t, int(100 * (1-float(i)/t))
            unique_total += i
    
    print "\nunique_total: %s" % unique_total
            
axis_stats()

# <headingcell level=1>

# REDUNDANCY FOR ALL TITLES & LABELS COMBINED

# <codecell>

all_unique_text, i, t = unique(".//title")
all_unique_text, i, t = unique(".//label", target=all_unique_text)
print len(all_unique_text)

# <markdowncell>

# ...less than previous total so small amount of text duplicated between different axes.
# 
# "all_unique_text" can now be used to remove redundancy by replacing xml text with index into sorted version...

# <codecell>

text_table = list(all_unique_text)
text_table.sort()

print "\n".join(text_table[:10])
print
print text_table.index("2nd Toe, Left") # ZERO-BASED INDEX!

# <markdowncell>

# Turns out there were axis/pos=3 without definitions...

# <codecell>

def xdefinition(e):
    global c1,c2
    c1 += 1
    try:
        return e.find('definition').text
    except:
        c2 += 1
        #print c1, e, e.attrib['pos']
        return None

# <markdowncell>

# Check for redundancy in definitions:

# <codecell>

#Check definitions
xaxes = root.iterfind(".//axis[@pos='3']")

# <codecell>

c1 = c2 = 0
defs = [xdefinition(a) for a in xaxes if a]
print len(defs)

# <codecell>

dset = set()
for d in defs:
    dset.add(d)
print len(dset), len(defs)

# <markdowncell>

# ... ~87% redundant. How many axis/pos=3 without definitions...

# <codecell>

print c2

# <markdowncell>

# See icd10-pcs-to-sqlite.ipynb/.py for sqlite db creation.

