CREATE TABLE IF NOT EXISTS PcsText(
    id integer,
	pcs_text text,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS PcsTable(
    id integer,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS PcsRow(
    id integer,
	table_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(table_id_fk) REFERENCES PcsTable(id)
);

CREATE TABLE IF NOT EXISTS PcsTableAxis(
    id integer,
	table_id_fk integer,
	pos integer,
	nvalues integer,
	title_text_id_fk integer,
	definition_text_id_fk text,
    PRIMARY KEY(id),
    FOREIGN KEY(table_id_fk) REFERENCES PcsTable(id),
    FOREIGN KEY(title_text_id_fk) REFERENCES PcsText(id)
    FOREIGN KEY(definition_text_id_fk) REFERENCES PcsText(id)
);

CREATE TABLE IF NOT EXISTS PcsRowAxis(
    id integer,
	row_id_fk integer,
	pos integer,
	nvalues integer,
	title_text_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(row_id_fk) REFERENCES PcsRow(id),
    FOREIGN KEY(title_text_id_fk) REFERENCES PcsText(id)
);

CREATE TABLE IF NOT EXISTS PcsTableAxisLabel(
    id integer,
	axis_id_fk integer,
	code text,
	label_text_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(axis_id_fk) REFERENCES PcsTableAxis(id)
    FOREIGN KEY(label_text_id_fk) REFERENCES PcsText(id)
);

CREATE TABLE IF NOT EXISTS PcsRowAxisLabel(
    id integer,
	axis_id_fk integer,
	code text,
	label_text_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(axis_id_fk) REFERENCES PcsRowAxis(id)
    FOREIGN KEY(label_text_id_fk) REFERENCES PcsText(id)
);

