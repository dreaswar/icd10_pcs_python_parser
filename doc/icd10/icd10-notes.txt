Derek O'Connell
4th April 2014

These are my personal discovery notes regarding the ICD-10-PCS XML database. The data supplier cms.gov (see link below) provides a wealth of information that I am still digesting and definitely not trying to reproduce here. Instead I'm aiming to provide an overview/summary useful to other AuShadha developers and for the translation of other data sources.

	CSM.GOV & ICD10 Info: http://www.cms.gov/Medicare/Coding/ICD10/2014-ICD-10-PCS.html

To my limited knowledge the two main uses of ICD-10-PCS is for statistics and billing. ICD-10-PCS forms a common basis for statistical analysis to compare/contrast different organisations and identify anomalies/trends. It is also a classification mechanism for the purpose of itemised billing for patients and insurers. Medical procedures are constantly refined and new ones developed. For these and other reasons you may wish to view ICD-10-PCS as a snapshot and/or a constant work-in-progress even though the data itself is not being constantly updated (annually?). 

The application and management of information related to ICD-10 and other medical coding systems is sometimes complex and requires in-depth experience. The general term used is "Clinical Coding" and there is the recognised profession of "Clinical Coder". It is important to realise that ICD-10-PCS is a human-centric coding since, as we will see, it has informal aspects that may affect how it used/managed within AuShadha. 

ICD-10-PCS codes are published by cms.gov in the form of coding-books/tables which are divided into "code pages". For an introduction see 2014-pcs-procedure-coding-system.pdf in the available downloads. An example of a code-page is provided here for convenience in ex-pg756-code-pg-302.png (in the same folder as these notes). A code consists of up to seven characters and designate:

	Code Character Position and Generic Title

	1	Section
	2	Body System
	3	Operation
	-
	4	Body Part
	5	Approach
	6	Device
	7	Qualifier

A code-page is identified by a unique combination of the first three characters and each page has four columns corresponding to the other four characters of the code. Each column has a number of rows and selection is made from each column independently to complete a coding. A particular procedure coding may take require less than seven characters therefore not all code combinations are valid ("Z" is the designated code to indicate "unused" but keep reading!). All this is represented in XML by "pcsTable", "axis", "pcsRow", "title" and "label" tags, along with various tag attributes. Here is an annotated outline of the XML:

	Outline of XML File Structure

	ICD10PCS.tabular
		version, 1
		title, 1
		pcsTable, 1..n
			axis, 3 (codes 1..3)
				@pos (== code-type)
				@values (number of labels)
				title, 1
					text
				label, 1 (for axes that are direct children of pcsTable's)
					@code (single alpha-digit)
					text
				definition, 1 (only for @pos==3, not mandatory)
					text
			pcsRow, 1..n
				@codes (total number of codes for this row = sum of axis/@values)
				axis, 4 (codes 4..7)
					@pos (== code-type)
					@values (number of labels)
					title, 1
						text
					label, 1..n (for axes that are direct children of pcsRow's)
						@code (single alpha-digit)
						text

Each of the seven code types are represented in XML by an "axis". Axes are commonly thought of as orthogonal and independent unless constraints are stated/imposed (eg, an XY scatter plot of random values for both X & Y). In ICD-10-PCS, while a selection can be made from any combination of column/rows, it is partially the responsibility of the medical practitioner doing the coding to make sensible choices. 

In addition, although generic titles are used as shown above, each and every instance of an axis can have it's own unique title despite being represented by the same code character position. For example, in the included code-page the title for code position six is "Substance" not "Device". Since there are no "title" codes it has to assumed that titles can be anything and therefore the title itself has to be carried forward into any translation (if only for displaying). See below for variants found in the current data set. The collary is that an "axis" primarily represents the position of a code character in the full coding (hence the location of associated information on an actual code-page) but has variable meaning.

These two aspects raise a key point: as a coding system ICD-10-PCS is neither internally consistent nor complete. In other words, external knowledge and expertise is needed in the construction and transcribing of a code and additional data (titles and labels) are needed to interpret a coding. This is the human-centric aspect mentioned above.

	Axis Title Variations

	1 Section
	-
	2 Body System
	2 Section Qualifier
	-
	3 Operation
	3 Modality
	3 Type
	-
	4 Body Part
	4 Body Region
	4 Body System
	4 Body System / Region
	4 Qualifier
	4 Treatment Site
	-
	5 Approach
	5 Contrast
	5 Duration
	5 Modality Qualifier
	5 Qualifier
	5 Radionuclide
	5 Type Qualifier
	-
	6 Device
	6 Equipment
	6 Function
	6 Function / Device
	6 Isotope
	6 Method
	6 Qualifier
	6 Substance
	-
	7 Qualifier

These uncoded variations are also seen for axis label text vs label code. For example "Z" (code position "unused") which you might expect to be very consistent has been found to have the following label variants:

	Axis Label Variants for "Z" Codes

	1	(not used)
	2	None
	3	(not used)
	4	Anatomical Region, Other
	4	Musculoskeletal System, All
	4	None
	5	None
	6	No Device
	6	None
	6	No Qualifier
	7	None
	7	No Qualifier

Clearly "Z" is also being used for other purposes so cannot be assumed.

Variations are also found in row labels and this raises the additional question of redundancy. A quick check of *unique* row labels compared to total number for each axis reveals percentage redundancies:  

	Per Axis, Unique Row Labels vs Total and Percent Redundancy

	1	16	845	98%
	2	58	845	93%
	3	109	845	87%
	4	1524	13261	89%
	5	220	5079	96%
	6	244	4695	95%
	7	326	2767	88%

This represents a very significant amount of redundancy that may not be too much of a problem for ICD-10-PCS at 2.5M but could be very problematic with something like Drugbase at 235M. My latest investigations reveal ~70% redundancy overall, a figure which if carried over to DrugBank would represent a saving of 165M!

So far I have created my own version of Easwar's parser which preserves the original parser's split of row axes into the four categories of "Body Part", "Approach", "Device" and "Qualifier", and outputs text, JSON, YAML and a SQLite DB based on this scheme. After realising the variants mentioned above I have created another XML-to-SQLite parser that drops these categories and instead divides axes & labels between pcsTable and pcsRow types. In this way it mirrors closely the original XML and avoids building in any assumptions. The XML-to-SQLite parser also deals with redundancy by extracting all text into a separate table and replacing any use in titles, labels and definitions with foreign keys pointing back to this table. This may also help with any future language translations. See icd10_xml_schema.sql for the current design of the new DB. JSON, YAML and text extracts to follow.


==================================================
BaseX (http://basex.org/)

I used BaseX to easily visualise the XML and test some xpath queries prior to implementing them in Python. The Python solutions were usually easier and faster but BaseX was still great for getting a feel for the data. Here are some of the xpath's I used:

To get code & text of "Operation" type axes ("&#10;" used for newlines):

	//axis[@pos="3"]/label/concat(./@code, " ",./text(), "&#10;")

An attempt at getting *unique* labels for "Body System" type axes:

	//axis[@pos="2"]/label[not(. = following::label)]/concat(string(@code), ' ', text(), "&#10;")

A simple list of "Body Part" titles:

	//axis[@pos="4"]/title/concat(., "&#10;")

Same but unique entries:

	//axis[@pos="4"]/title[not(. = following::title)]/concat(., "&#10;")

"Operation" type axes that *don't* have a "definition" tag (yes there are 50+):

	//axis/.[@pos='3' and not(definition) ]

An attempt to extract all the text used in titles & labels (again, much easy in Python):

*[//title/text() or //label/text()]

==================================================
QUICK REDUNDANCY ESTIMATE

Using icd10pcs_tabular_2014.txt output from my version of Easwar's parser:

	Summary:
		lines:	26073
			tables:	  845
			rows:	 2022
			labels:	23206
		unique:	 4530
		redundancy:	~83%

	Commands used (same order as above):

		$ wc -l icd10pcs_tabular_2014.txt
		26073

		$ grep -P "\t\(T" icd10pcs_tabular_2014.txt | wc -l
		845

		$ grep -P "\t\t\(R" icd10pcs_tabular_2014.txt | wc -l
		2022

		$ grep -P "\t\t\t\(" icd10pcs_tabular_2014.txt | wc -l
		23206

		$ sort -u icd10pcs_tabular_2014.txt | wc -l
		4530

	~83% redundant





