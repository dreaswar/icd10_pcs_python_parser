# -*- coding = utf-8 -*-
"""
Created on Tue Mar 18 09:57:30 2014

@author = doc
"""

import os
#from __future__ import print_function
#import json
#from collections import OrderedDict

from common.common import *

DEBUG = False
DEBUG_PREFIX = "TXT: "

DATADIR  = 'data'
FILENAME = 'icd10pcs'
FILEEXT  = '.txt'
FILEPATH = os.path.join(DATADIR, FILENAME+FILEEXT)

TXTFILE = None

## MODELS
#MOD_PREFIX = "icd10_pcs."
#MOD_TABLE     = MOD_PREFIX + "PcsTable"
#MOD_ROW       = MOD_PREFIX + "PcsRow"
#MOD_BODYPART  = MOD_PREFIX + "BodyPart"
#MOD_APPROACH  = MOD_PREFIX + "Approach"
#MOD_DEVICE    = MOD_PREFIX + "Device"
#MOD_QUALIFIER = MOD_PREFIX + "Qualifier"
##
###LABEL_TABLES = ('body_part', 'approach', 'device', 'qualifier')
##
#LABEL_MODELS = {'bodyPart' : MOD_BODYPART, 
#                'approach' : MOD_APPROACH,
#                'device'   : MOD_DEVICE,
#                'qualifier': MOD_QUALIFIER
#               }

CHAR_TAB = "\t"
CHAR_SPACE = " "
CHAR_DASH = "-"
CHAR_NEWLINE = "\n"
CHAR_BSLASH = "\\"
CHAR_FSLASH = "/"

INDENT_CHAR = CHAR_TAB
INDENT_REPEAT = 1

CHUNK_DIVIDER = CHAR_FSLASH

translation = None
level = 0
indent = ""


def debug(msg):
    """Module debugging"""
    if DEBUG:
        print "{}: {}".format(__name__,msg)


#==========================================================

def init(datadir, filestub):
    """Translator initialization"""
    global DATADIR, FILEPATH, TXTFILE, translation
    DATADIR, FILESTUB = datadir, filestub
    FILEPATH = os.path.join(DATADIR, filestub+FILEEXT)
    TXTFILE = open(FILEPATH, 'wb', 1)
    translation = ""

def set_level(new_level):
    """Set level"""
    global level, indent
    if new_level > level:
        pass
    elif new_level < level:
        pass
    level = new_level
    indent = INDENT_CHAR * INDENT_REPEAT * level

#def translate_pcs_table(pcs_table_id, section, body_system, operation):
def translate_pcs_table(chunk):
    """Translate chunk"""
#    fields = OrderedDict()
#    fields['table']       = "pcsTable"
#    fields['section']     = chunk["section
#    fields['body_system'] = body_system
#    fields['operation']   = operation
    #fields['sec_id']      = str(pcs_table_id)
    #return new_chunk(fields, MOD_TABLE, pcs_table_id)
    f = chunk['fields']
#    return ("pcsTable: ", f['section'], f['body_system'], f['operation'])
    return (f['section'], f['body_system'], f['operation'])

#def translate_pcs_row(pcs_row_id, pcs_table_id):
def translate_pcs_row(chunk):
    """Translate chunk"""
#    fields = OrderedDict()
#    fields['table']       = "pcsRow"
#    #fields['pcsRow_id']   = str(pcs_row_id)
#    #fields['pcsTable_fk'] = str(pcs_table_id)
#    add_chunk(new_chunk(fields, MOD_ROW, pcs_row_id))
#    return True
    f = chunk['fields']
#    return ("pcsRow: ", str(f['pcsTable_fk']), str(f['pcsRow_id']))
    return (str(f['pcsTable_fk']), str(f['pcsRow_id']))
    
#def translate_pcs_label(table, label_id, pcs_row_fk, label_text):
def translate_pcs_label(chunk):
    """Translate chunk"""
#    fields = OrderedDict()
#    fields['table']     = table
#    #fields['pcsRow_fk'] = str(pcs_row_fk)
#    fields[table]       = label_text
#    add_chunk(new_chunk(fields, LABEL_MODELS[table], label_id))
#    return True
    f = chunk['fields']
#    return (chunk['model']+": ", f[chunk['model']])
    return (f[chunk['model']], )

def model_text(model):
    return {
            MODEL_PCSTABLE  : "(T)",
            MODEL_PCSROW    : "(R)",
            MODEL_BODYPART  : "(B)",
            MODEL_APPROACH  : "(A)",
            MODEL_DEVICE    : "(D)",
            MODEL_QUALIFIER : "(Q)"
           }[model]
           
def new_chunk(level, chunk_type, chunk):
#def new_chunk(fields, model, pk):
    """Create a new chunk"""
#    chunk = OrderedDict()
#    chunk['fields'] = fields
#    chunk['model']  = model
#    chunk['pk']     = int(pk)
#    chunk = fields.values()
#    chunk.extend((model, pk))
#    return chunk
    
    if chunk_type == MODEL_PCSTABLE:
        fields = translate_pcs_table(chunk)
    elif chunk_type == MODEL_PCSROW:
        fields = translate_pcs_row(chunk)
    else: # MODEL_PCSLABEL
        fields = translate_pcs_label(chunk)
#    trans = indent + CHUNK_DIVIDER.join(str(v) 
#                                        for v in chunk['fields'].values())
    trans = indent + model_text(chunk['model']) + ": " + CHUNK_DIVIDER.join(fields)
    TXTFILE.writelines(trans+CHAR_NEWLINE)
    
def save(translation):
    """Save translation to file"""
    global TXTFILE
#    with file(FILEPATH,'w') as f:
#        f.write(json.dumps(json_data, indent = 4))
    if TXTFILE:
        TXTFILE.close()
        TXTFILE = None


def clean_up():
    """Translator clean up"""
    #save_translation(translation)


#==========================================================
if __name__ == '__main__':
    pass